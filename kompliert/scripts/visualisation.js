let maxValue = 40;
let numberOfBars = 12;
let leftIndex = null;
let rightIndex = null;
let listBar = null;
let range = document.getElementById('range');
let tlAnimationStart = gsap.timeline();
let sortButton = document.getElementById('sortButton');
let newButton = document.getElementById('newButton');
let cancelButton = document.getElementById('cancelButton');
let msg = document.getElementById('msg');
let msg2 = document.getElementById('msg2');
let msg3 = document.getElementById('msg3');


function status(){
    msg2.classList.remove('show');
    msg.classList.add('show');
    gsap.from(msg,{xPercent: -100, duration: 0.9});
    newButton.disabled = false;
    cancelButton.style.display = 'none';
}


function responsiveChange(node, size){
    if(window.matchMedia('(max-width: 681px)').matches){
        node.setAttribute('style','width: '+size+'%;');
    } else {
        node.setAttribute('style','height: '+size+'%;');
    }
}


function buildNode(value){
    let node = document.createElement('div');
    node.innerText = value;
    let size = value * 100/ maxValue+2;
    node.setAttribute('data-value', value);
    responsiveChange(node, size);
    window.addEventListener('resize', function (){
        responsiveChange(node,size);
        msg.classList.remove('show')
    })
    node.setAttribute('class', 'bar');
    return node;
}

function randomNode(){
    for(let i=0; i<numberOfBars; i++){
        let random = Math.round((maxValue-1) * Math.random())+1;
        range.appendChild(buildNode(random));
    }
}

function init() {
    randomNode();


    sortButton.addEventListener('click', function () {
        listBar = document.getElementsByClassName('bar');
        msg2.classList.add('show')
        gsap.from(msg2,{xPercent: -100, duration: 0.9})
        msg3.classList.remove('show');
        rightIndex = listBar.length - 1;
        leftIndex = 0;
        quicksort(listBar, leftIndex, rightIndex).then(()=> status())
        sortButton.style.display = 'none';
        cancelButton.style.display = 'inline-block';
        newButton.disabled = true;
    });

    newButton.addEventListener('click', function () {
        sortButton.style.display = 'inline-block';
        cancelButton.style.display = 'none';
        while (range.firstChild) {
            range.removeChild(range.firstChild);
        }
        msg.classList.remove('show');
        msg3.classList.remove('show');
        randomNode();
        animationStart();
    })

    cancelButton.addEventListener('click', async function () {
        await gsap.killTweensOf('.bar');
        newButton.disabled = false;
        msg2.classList.remove('show');
        msg3.classList.add('show');
        gsap.from(msg3,{xPercent: -100, duration: 0.9})
        cancelButton.style.display = 'none';
        sortButton.style.display = 'inline-block';
        await resetAfterIter(listBar);
    })

}
function animationStart(){
    tlAnimationStart
        .add('startTl')
        .set('.bar', {autoAlpha:0, x:+50})
        .from('#container', {duration: 2.5, ease: 'expo.Out(3)', x:+500, autoAlpha:0}, 'startTl')
        .to('.bar', {duration: 0.9, stagger: 0.2, autoAlpha:1, ease:'expo.Out(3)', x:0}, 'startTl')
    ;
}

async function quicksort(listBar, left, right,){
    if (left < right) {
        let index = await partition(listBar, left, right);
        await quicksort(listBar, left, index - 1);
        await quicksort(listBar, index + 1, right);
    }
}

async function partition(listBar ,left, right){

    let i = left;
    let j = right-1;

    let pivot = listBar[right];
    await gsap.to(pivot,{
        background: '#F4AA00',
        duration: 1
    })
    while(i < j){

        while (i < right && parseInt(listBar[i].dataset.value)  < parseInt(pivot.dataset.value)){
            await gsap.to(listBar[i],{
                background: '#00B8E4',
                duration: 1,
            })
            i++
        }


        while (j > left && parseInt(listBar[j].dataset.value) >= parseInt(pivot.dataset.value)){
            await gsap.to(listBar[j],{
                background: '#9C132E',
                duration: 1
            })
            j--;

        }

        if(i < j) {
            await swap(listBar[i], listBar[j]);
        }

    }
    if(parseInt(listBar[i].dataset.value) > parseInt(pivot.dataset.value)){
        await swap(listBar[i],listBar[right]);
        await gsap.to(listBar[right],{
            background: '#9C132E',
            duration: 1
        })

    }

    await resetAfterIter(listBar);

    return i;
}

async function swap(nodeLeft, nodeRight) {
    let temp = document.createElement('div');
    nodeLeft.before(temp);
    nodeRight.before(nodeLeft);
    temp.replaceWith(nodeRight);
}

async function resetAfterIter(listBar){
    for(let i = 0; i<numberOfBars; i++){
        gsap.to(listBar[i],{ background: '#002878'});
    }
}

window.addEventListener('DOMContentLoaded', function (){
    init();
});
