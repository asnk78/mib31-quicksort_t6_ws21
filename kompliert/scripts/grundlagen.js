let textBox = document.getElementById("textBox");
textBox.style.textAlign = "center";
let boxOne = document.getElementById("boxOne");
let boxTwo = document.getElementById("boxTwo");
let boxThree = document.getElementById("boxThree");
let boxFour = document.getElementById("boxFour");
let boxFive = document.getElementById("boxFive");
let nextBtn = document.getElementById("nextBtn");
let thmRed = "#9C132E";
let thmBlue = "#002878";
let thmYellow = "#F4AA00";
let thmGreen = "#80BA24";
let thmGrey = "#4A5C66";
let zaehler = 0;
let text = ["1. Als erstes wählen wir unser Pivot Element, wir nehmen mal das erste Element der Liste (3). ",
    "2. Jetzt wird das Pivot Element in die Mitte verschoben!",
    "3. Daraufhin werden die Zahlen von vorne nach hinten durchgegangen, die kleineren Zahlen kommen auf die linke, die größeren auf die rechte Seite des Pivot Elements!",
    "4. Nun werden von der linken (1) und rechten (4) Seite des Pivots die ersten Elemente als weitere Pivot Elemente markiert!",
    "5. Die linke Seite ist fertig sortiert, auf der rechten werden die übrigen Elemente mit dem neuen Pivot-Element (4) verglichen.",
    "6. Nun wird auf der rechten Seite wieder das erste Element (7) als neues Pivot markiert und mit der letzten Zahl verglichen.",
    "7. Zum Schluss wird das verbleibende Element (5) als Pivot markiert. Somit sind nun alle Zahlen Pivot Elemente gewesen, die Zahlenfolge ist sortiert!",""
];


async function stepByStep(){

    if(zaehler===0){
        setText(zaehler);
        nextBtn.textContent = "Nächster Schritt";
        boxOne.style.backgroundColor = thmRed;
        marker("#boxOne");
    }
    if(zaehler===1){
        setText(zaehler);
        gsap.to("#boxOne", {x: 2*x, duration: 2})
        gsap.to("#boxTwo, #boxThree", {x: -x, duration: 2})
    }
    if(zaehler===2){
        setText(zaehler);
        await sleep(1500);
        boxTwo.style.backgroundColor = thmYellow;
        await sleep(1500);
        boxThree.style.backgroundColor = thmYellow;
        await sleep(1500);
        gsap.to("#boxOne", {x: x, duration: 2})
        gsap.to("#boxThree", {x: 0, duration: 2})
        await sleep(2500);
        boxFour.style.backgroundColor = thmYellow;
        await sleep(1500);
        boxFive.style.backgroundColor = thmYellow;
    }
    if(zaehler===3){
        setText(zaehler);
        boxFour.style.backgroundColor = thmBlue;
        boxFive.style.backgroundColor = thmBlue;
        marker("#boxTwo");
        boxTwo.style.backgroundColor = thmRed;
        await sleep(1500);
        marker("#boxThree");
        boxThree.style.backgroundColor = thmRed;

    }
    if(zaehler===4){
        setText(zaehler);
        marker("#boxFour");
        boxFour.style.backgroundColor = thmYellow;
        await sleep(3000);
        marker("#boxFive");
        boxFive.style.backgroundColor = thmYellow;
    }
    if(zaehler===5){
        setText(zaehler);
        boxFour.style.backgroundColor = thmRed;
        marker("#boxFive");
        await sleep(3000);
        gsap.to("#boxFive", {x: -x, duration: 2});
        gsap.to("#boxFour", {x: x, duration: 2})
    }
    if(zaehler===6){
        setText(zaehler);
        marker("#boxFive");
        boxFive.style.backgroundColor = thmRed;
        nextBtn.innerHTML = '<i class="far fa-redo"></i> Neustart'
    }
    if(zaehler>6){
        setText(zaehler);
        boxOne.style.backgroundColor = thmBlue;
        boxTwo.style.backgroundColor = thmBlue;
        boxThree.style.backgroundColor = thmBlue;
        boxFour.style.backgroundColor = thmBlue;
        boxFive.style.backgroundColor = thmBlue;
        gsap.to("#boxOne, #boxTwo, #boxThree, #boxFour, #boxFive", {x: 0, duration: 2})
        nextBtn.textContent = "Start";
        zaehler=-1;
    }
    zaehler++;
}

    let ablauf = gsap.timeline({repeat:-1, repeatDelay: 60});
    ablauf.to("#ablaufEins",{duration: 3, text: "Pivot Element wählen!"})
    ablauf.to("#ablaufZwei",{duration: 2, delay: 1,text: "Pivot Element in die Mitte verschieben!"})
    ablauf.to("#ablaufDrei",{duration: 3, delay: 1,text: "Zahlen von vorne nach hinten durchgehen, kleinere kommen auf die linke, größere auf die rechte Seite des Pivot Elements!"})
    ablauf.to("#ablaufVier",{duration: 3, delay: 1,text: "Nun werden die jeweils ersten Elemente der zwei Teillisten als neue Pivot Elemente definiert."})
    ablauf.to("#ablaufFünf",{duration: 3, delay: 1,text: "Jetzt werden die zwei Teillisten wieder von vorne beginnend mit dem Pivot verglichen."})
    ablauf.to("#ablaufSechs",{duration: 3, delay: 1,text: "Ab jetzt wiederholen sich die Schritte von Punkt drei bis fünf bis die Liste fertig sortiert ist."})


function marker (id){
    gsap.to(id,{
        y:-20,
        ease: "circ.out",
        delay: .5
    })
    gsap.to(id,{
        y:0,
        ease: "linear",
        delay: 2.5
    })
}
function setText(i){
    gsap.to('#textBox', {duration: 3, text: text[i]})
}
function sleep(ms) {
    return new Promise(
        resolve => setTimeout(resolve, ms)
    );
}

