gsap.registerPlugin(ScrollTrigger,ScrollToPlugin);

let textAnimationGrund1;
let textAnimationGrund2;
let textAnimationGrund3;



let btt = document.querySelector('.topButton');
gsap.to(btt, {
    autoAlpha: 1,
    scrollTrigger: {
        start: 'top -10%',
        end: 'top -20%',
        scrub: true
    }
});

btt.addEventListener('click',function (e){
    e.preventDefault();
    gsap.to(window, { scrollTo: {y: 0 }, duration: 1});
})


let navItems = document.querySelectorAll('.navItem');
let sections = document.querySelectorAll('.contentSection');
sections.forEach((section)=>{
    gsap.from(section,{
        xPercent: -150,
        scrollTrigger: {
            trigger: section,
            start: 'bottom bottom',
            end: 'top top',
            toggleActions: 'play none none reverse',
            onEnter: ()=> {
                if(section.id === 'grundlagen'){
                    textAnimationGrund1 = gsap.from(grundlText, 1.7,{
                        y:-600,
                        x:-25,
                        ease: "bounce.out",
                        delay: .9,
                        stagger: {
                            amount: 0.1
                        }
                    });

                    textAnimationGrund2 = gsap.from(grundlText2, 1.7,{
                        y:-600,
                        x:-25,
                        ease: "bounce.out",
                        delay: .9,
                        stagger: {
                            amount: 0.1
                        }
                    });
                    textAnimationGrund3 = gsap.from(grundlText3, 1.7,{
                        y:-600,
                        x:-25,
                        ease: "bounce.out",
                        delay: .9,
                        stagger: {
                            amount: 0.1
                        }
                    });
                    ablauf.play();
                }

            },
            onLeave: ()=> {
                if(section.id === 'einführung') {
                    animate_text.reverse()
                    animate_text2.reverse();
                    boxIntro.reverse();
                    boxMove.pause();
                }
                if(section.id ==='grundlagen'){
                    textAnimationGrund1.reverse();
                    textAnimationGrund2.reverse();
                    textAnimationGrund3.reverse();
                    ablauf.pause();
                }

                if(section.id=== 'stepbystep'){
                    boxSbS.reverse();
                }
            },
            onEnterBack: ()=> {
                if(section.id === 'einführung') {
                    animate_text.play()
                    animate_text2.play();
                    boxIntro.play();
                    boxMove.play();
                }
                if(section.id=== 'grundlagen'){
                    textAnimationGrund1.play();
                    textAnimationGrund2.play();
                    textAnimationGrund3.play();
                    ablauf.play();
                }
                if(section.id=== 'stepbystep'){
                    boxSbS.play();
                }
            },

        },
        duration: 1,
    })
})

navItems.forEach((item)=>{
    item.addEventListener('click', function(e){
        e.preventDefault();
        gsap.to(window,{ duration: 1,
            scrollTo: {y: item.getAttribute('href'), offsetY: 20 }
        })

    })
})


navItems.forEach((item) =>{
    sections.forEach((section)=> {
        let current = '#'+section.getAttribute('id');
        if(item.getAttribute('href')=== current ) {
            gsap.to(item, {
                scrollTrigger: {
                    trigger: section,
                    start: 'bottom bottom',
                    end: 'top top',
                    toggleClass: {targets: item, className: 'current'},
                },
            })

        } else{
            item.className = 'navItem';
        }
    })
});



let header = document.querySelector('#header');
let nav = document.querySelector('.navMenu');

gsap.to(header, {
    yPercent: -200,
    scrollTrigger:{
        start: 'top -7.5%',
        toggleActions: 'play none none reverse'
    },
    duration: 0.5
});


gsap.to(nav, {
    y: ()=> - header.clientHeight ,
    scrollTrigger: {
        start: 'top -5%',
        toggleActions: 'play none none reverse',
        invalidateOnRefresh: true
    },
    duration: 0.5
});


let footer = document.querySelector('#footer');

gsap.from(footer,{
    xPercent: -100,
    scrollTrigger:{
        start: 'bottom bottom',
        toggleActions: 'play none none reverse',
        onEnter: ()=> {
            animate_footerText.play();
        },
        onLeaveBack: ()=> {
            animate_footerText.reverse();
        }
    },
    duration: 1,
});
