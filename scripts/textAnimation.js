let introText = document.getElementById('introText');
let introText2 = document.getElementById('introText2');
let grundlText = document.getElementById('grundlagentext');
let grundlText2 = document.getElementById('grundlagentext2');
let grundlText3 = document.getElementById('grundlagentext3');
let footerText = document.getElementById('footerText');

const animate_text = gsap.from(introText, 1.7,{
    y:-600,
    x:-25,
    ease: "bounce.out",
    delay: .9,
    stagger: {
        amount: 0.1
    }
});
const animate_text2 = gsap.from(introText2, 1.7,{
    y:-600,
    x:-25,
    ease: "bounce.out",
    delay: .9,
    stagger: {
        amount: 0.1
    }
});



const animate_footerText = gsap.from(footerText, 1.7,{
    y:-600,
    x:-25,
    ease: "bounce.out",
    delay: .9,
    stagger: {
        amount: 0.1
    }
});





const boxIntro = gsap.from("#boxes", 2, {
    y:500,
    x:0,
    ease: "elastic.out(1, 0.5)",
    delay: 1
});

const boxSbS = gsap.from("#boxes2", 2, {
    y:500,
    x:0,
    ease: "elastic.out(1, 0.5)",
    delay: 1
});
let width = document.getElementById("boxes").offsetWidth;
let x = width / 5.8;
let boxMove = gsap.timeline({repeat:-1, repeatDelay: 5});
boxMove.delay(2)
boxMove.to(".box.two", {x: -x, duration: 2})
boxMove.to(".box.one", {x: x, duration: 2})
boxMove.to(".box.four", {x: x, duration: 2})
boxMove.to(".box.five", {x: -x, duration: 2})
boxMove.to(".box.three", {x: x, duration: 2})
boxMove.to(".box.five", {x: -x-x, duration: 2})
