let question = document.getElementById("question");
let a1 = document.getElementById("a1");
let a2 = document.getElementById("a2");
let a3 = document.getElementById("a3");
let a4 = document.getElementById("a4");
let answerClass = document.getElementsByClassName("answers");
let restartBtn = document.getElementById("restartBtn");
restartBtn.style.visibility = "hidden";
let a,b,c,d, whichQuestion = 0;
let answeredQuestions = [];
let questions = [
    ["Was ist das Hauptelement des Quicksort?", "Das Pivot Element", "keins davon", "Der Zeiger", "Es gibt kein Hauptelement"],
    ["Zu welcher Art von Sortieralgorithmen gehört der Quicksort?", "rekursiv", "stabil", "ineffizient", "keins davon"],
    ["Warum wird das Teile-und-herrsche Prinzip angewendet?", "Um das große Problem in kleinere Teilprobleme aufzulösen", "Um die Mächtigkeit des Algorithmus darzustellen", "Es hat keine tiefere Bedeutung", "Keine Ahnung"],
    ["Wozu werden Algorithmen wie z.B. der Quicksort verwendet?", "Um Zahlenfolgen schnell und effizient zu ordnen", "Um große Zahlenfolgen zu zählen", "Sie haben kein gezieltes Einsatzspektrum", "keins davon"],
    ["Welche der folgenden Aussagen ist wahr:", "keine davon", "Der Quicksort ist ein stabiler Algorithmus", "Der Quicksort ist nicht vergleichsbasiert", "Der Quicksort ist in der Regel langsam"]
];
setQuestionsAndAnswers()

function setQuestionsAndAnswers(){
    let isSet = true;
    while(isSet){
        whichQuestion = Math.floor((Math.random() * questions.length));
        a = Math.floor(1 + (Math.random() * 4));
        b = Math.floor(1 + (Math.random() * 4));
        c = Math.floor(1 + (Math.random() * 4));
        d = Math.floor(1 + (Math.random() * 4));
        if(a !== b && a!== c && b!==c && d!==a && d!==b && d!==c && !answeredQuestions.includes(whichQuestion)){
            isSet=false;
        }
    }
    question.textContent = questions[whichQuestion][0];
    a1.textContent = "A. " + questions [whichQuestion][a];
    a2.textContent = "B. " + questions [whichQuestion][b];
    a3.textContent = "C. " + questions [whichQuestion][c];
    a4.textContent = "D. " + questions [whichQuestion][d];
}

async function checkAnswer(answer) {
    if (answer.innerText.includes(questions[whichQuestion][1])) {
        question.textContent = "Klasse, das war richtig!"
        answer.style.backgroundColor = thmGreen;
        await sleep(3000);
        answeredQuestions.push(whichQuestion);
        if (answeredQuestions.length === questions.length) {
            question.textContent = "Super, du hast alle Fragen richtig beantwortet! Jetzt hast du schon ein solides Grundwissen über den Quicksort!";
            for(let item of answerClass){
                item.style.visibility = 'hidden';
            }
            restartBtn.style.visibility = "visible";
        } else {
            setQuestionsAndAnswers();
        }
    } else {
        question.textContent = "Das war leider falsch, probiere es nochmal!";
        answer.style.backgroundColor = thmRed;
        wiggleElement(answer)
        await sleep(3000);
        question.textContent = questions[whichQuestion][0];
    }
    answer.style.backgroundColor = thmGrey;
}
function restart(){
    answeredQuestions = [];
    setQuestionsAndAnswers();
    for(let item of answerClass){
        item.style.visibility = 'visible';
    }
    restartBtn.style.visibility = "hidden";
}
function wiggleElement(element){
    const tlWiggle = gsap.timeline();
    let id = "#" + element.id;
    tlWiggle.to(id, {rotate: 5, x: randomMovement, y: randomMovement, duration: 0.1 , transformOrigin:"50% 50%"})
    tlWiggle.to(id, {rotate: 0, x: randomMovement, y: randomMovement, duration: 0.1 , transformOrigin:"50% 50%"})
    tlWiggle.to(id, {rotate: -5, x: randomMovement, y: randomMovement, duration: 0.1 , transformOrigin:"50% 50%"})
    tlWiggle.to(id, {rotate: 0, x: randomMovement, y: randomMovement, duration: 0.1 , transformOrigin:"50% 50%"})
    tlWiggle.to(id, {rotate: 5, x: randomMovement, y: randomMovement, duration: 0.1 , transformOrigin:"50% 50%"})
    tlWiggle.to(id, {rotate: 0, x: 0, y: 0, duration: 0.1 , transformOrigin:"50% 50%"})
}
function randomMovement (){
    return (Math.random() * 2) + 5;
}
